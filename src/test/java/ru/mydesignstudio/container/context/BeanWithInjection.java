package ru.mydesignstudio.container.context;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public class BeanWithInjection {
    @Inject
    private InjectableBean injectableBean;
    private boolean initWasCalled = false;

    @PostConstruct
    public void init() {
        assert injectableBean != null;
        initWasCalled = true;
    }

    public InjectableBean getInjectableBean() {
        return injectableBean;
    }

    public void setInjectableBean(InjectableBean injectableBean) {
        this.injectableBean = injectableBean;
    }

    public boolean isInitWasCalled() {
        return initWasCalled;
    }

    public void setInitWasCalled(boolean initWasCalled) {
        this.initWasCalled = initWasCalled;
    }
}
