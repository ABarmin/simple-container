package ru.mydesignstudio.container.context;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public class ClassPathAnnotationApplicationContextTest {
    private final ApplicationContext applicationContext = new ClassPathAnnotationApplicationContext();

    @Test
    public void tryGetBeanByName() {
        final NamedBeanTest bean = applicationContext.getBean("someName", NamedBeanTest.class);
        assertNotNull(bean);
    }

    @Test
    public void tryToGetBeanWithDependency() {
        final BeanWithInjection bean = applicationContext.getBean(BeanWithInjection.class);
        assertNotNull(bean);
        assertNotNull(bean.getInjectableBean());
        assertTrue(bean.isInitWasCalled());
    }
}