package ru.mydesignstudio.container.context;

import org.junit.Test;
import ru.mydesignstudio.container.definition.BeanDefinition;
import ru.mydesignstudio.container.utils.StringUtils;

import java.util.Collection;

import static org.junit.Assert.*;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public class ClassPathAnnotationBeadDefinitionSourceTest {
    private BeanDefinitionSource source = new ClassPathAnnotationBeadDefinitionSource();

    @Test
    public void read() {
        final Collection<BeanDefinition> definitions = source.read();
        assertNotNull(definitions);
        assertFalse(definitions.isEmpty());
    }

    @Test
    public void tryToFindNamedBean() {
        final Collection<BeanDefinition> definitions = source.read();
        boolean found = false;
        for (BeanDefinition definition : definitions) {
            if (definition.getId().equals("someName")) {
                found = true;
                break;
            }
        }
        assertTrue(found);
    }

    @Test
    public void tryToFindUnnamedBean() {
        final Collection<BeanDefinition> definitions = source.read();
        boolean found = false;
        for (BeanDefinition definition : definitions) {
            if (definition.getId().equals("unnamedBeanTest")) {
                found = true;
                break;
            }
        }
        assertTrue(found);
    }

    @Test
    public void tryToFindWithInterface() {
        final Collection<BeanDefinition> definitions = source.read();
        boolean found = false;
        for (BeanDefinition definition : definitions) {
            for (String name : definition.getName()) {
                if (name.equals("someInterface")) {
                    found = true;
                    break;
                }
            }
        }
        assertTrue(found);
    }

    @Test
    public void tryToFindBeanWithInitMethod() {
        final Collection<BeanDefinition> definitions = source.read();
        boolean found = false;
        for (BeanDefinition definition : definitions) {
            if (StringUtils.isNotEmpty(definition.getInitMethod())) {
                found = true;
                break;
            }
        }
        assertTrue(found);
    }

    @Test
    public void tryToFindBeanWithDestroyMethod() {
        final Collection<BeanDefinition> definitions = source.read();
        boolean found = false;
        for (BeanDefinition definition : definitions) {
            if (StringUtils.isNotEmpty(definition.getDestroyMethod())) {
                found = true;
                break;
            }
        }
        assertTrue(found);
    }

    @Test
    public void tryToFindBeanWithProperties() {
        final BeanDefinition definition = source.read().stream()
                .filter(def -> def.getId().equalsIgnoreCase("beanWithPropertiesTest"))
                .findFirst()
                .orElseThrow(() -> new RuntimeException());
        assertNotNull(definition);
        assertFalse(definition.getProperties().isEmpty());
    }
}