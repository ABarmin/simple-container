package ru.mydesignstudio.container.context;

import java.io.Serializable;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public class ClassWithInterfaces implements SomeInterface, Serializable {
}
