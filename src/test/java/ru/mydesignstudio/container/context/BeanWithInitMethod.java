package ru.mydesignstudio.container.context;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public class BeanWithInitMethod {
    @PostConstruct
    public void init() {

    }

    @PreDestroy
    public void destroy() {

    }
}
