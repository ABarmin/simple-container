package ru.mydesignstudio.container.jdbc;

import org.junit.Test;
import ru.mydesignstudio.container.context.ApplicationContext;
import ru.mydesignstudio.container.context.ClassPathAnnotationApplicationContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public class DriverManagerDataSourceTest {
    @Test
    public void testGetDriverManager() throws Exception {
        final ApplicationContext context = new ClassPathAnnotationApplicationContext();
        final JdbcTemplate jdbcTemplate = context.getBean(JdbcTemplate.class);
        jdbcTemplate.executeQuery("DROP TABLE TEST IF EXISTS;");
        jdbcTemplate.executeQuery("CREATE TABLE TEST (ID INT PRIMARY KEY, NAME VARCHAR(255));");
        jdbcTemplate.executeUpdate("insert into TEST values (?, ?)", new Object[]{
                1, "test"
        });
        final Collection<Row> rows = jdbcTemplate.queryForList("select * from TEST", new RowMapper<Row>() {
            @Override
            public Row map(ResultSet resultSet) throws SQLException {
                final Row row = new Row();
                row.setId(resultSet.getInt("ID"));
                row.setName(resultSet.getString("NAME"));
                return row;
            }
        });
        System.out.println(rows);
        assert !rows.isEmpty();
    }

}