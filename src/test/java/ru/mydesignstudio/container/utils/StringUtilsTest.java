package ru.mydesignstudio.container.utils;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public class StringUtilsTest {

    @Test
    public void isEmpty() {
        assertTrue(StringUtils.isEmpty(""));
        assertTrue(StringUtils.isEmpty(" "));
    }

    @Test
    public void isNotEmpty() {
        assertTrue(StringUtils.isNotEmpty(" 1"));
    }
}