package ru.mydesignstudio.container.definition;

import ru.mydesignstudio.container.context.Ordered;

import java.util.ArrayList;
import java.util.Collection;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public class BeanDefinition {
    /**
     * Bean's id.
     * Unique and required field.
     */
    private String id;

    /**
     * Possible bean names. Could be undefined.
     */
    private Collection<String> name = new ArrayList<>();

    /**
     * Interfaces that implements bean.
     */
    private Collection<String> interfaces = new ArrayList<>();

    /**
     * Class to create bean.
     */
    private String beanClass;

    /**
     * Method to use for class initialization.
     */
    private String initMethod;

    /**
     * Method to use for class destruction.
     */
    private String destroyMethod;

    /**
     * Bean scope.
     */
    private String scope;

    /**
     * Bean properties.
     */
    private Collection<BeanPropertyDefinition> properties = new ArrayList<>();

    /**
     * Bean order.
     */
    private int order = Ordered.DEFAULT_ORDER;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Collection<String> getName() {
        return name;
    }

    public void setName(Collection<String> name) {
        this.name = name;
    }

    public String getBeanClass() {
        return beanClass;
    }

    public void setBeanClass(String beanClass) {
        this.beanClass = beanClass;
    }

    public String getInitMethod() {
        return initMethod;
    }

    public void setInitMethod(String initMethod) {
        this.initMethod = initMethod;
    }

    public String getDestroyMethod() {
        return destroyMethod;
    }

    public void setDestroyMethod(String destroyMethod) {
        this.destroyMethod = destroyMethod;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public Collection<BeanPropertyDefinition> getProperties() {
        return properties;
    }

    public void setProperties(Collection<BeanPropertyDefinition> properties) {
        this.properties = properties;
    }

    public Collection<String> getInterfaces() {
        return interfaces;
    }

    public void setInterfaces(Collection<String> interfaces) {
        this.interfaces = interfaces;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
