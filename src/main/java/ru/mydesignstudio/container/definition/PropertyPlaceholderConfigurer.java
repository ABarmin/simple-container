package ru.mydesignstudio.container.definition;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.util.Properties;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
@Named
public class PropertyPlaceholderConfigurer {
    private final String location = "/configuration.properties";

    private final Properties properties = new Properties();

    @PostConstruct
    public void init() {
        try (final InputStream inputStream = getClass().getResourceAsStream("/configuration.properties")) {
            properties.load(inputStream);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String getProperty(String property) {
        return properties.getProperty(property);
    }
}
