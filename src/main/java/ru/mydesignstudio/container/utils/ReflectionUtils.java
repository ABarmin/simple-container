package ru.mydesignstudio.container.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public class ReflectionUtils {
    public static void setAccessible(Object instance, String field) {
        try {
            instance.getClass().getDeclaredField(field).setAccessible(true);
        } catch (Exception e) {
            throw new RuntimeException("Can't set field accessible", e);
        }
    }

    public static void setField(Object instance, String field, Object value) {
        try {
            final Field fieldInstance = instance.getClass().getDeclaredField(field);
            fieldInstance.setAccessible(true);
            fieldInstance.set(instance, value);
        } catch (Exception e) {
            throw new RuntimeException("Can't set field value", e);
        }
    }

    public static Object invokeMethod(Object instance, String method, Object... args) {
        try {
            final Method declaredMethod = instance.getClass().getDeclaredMethod(method);
            declaredMethod.setAccessible(true);
            return declaredMethod.invoke(instance, args);
        } catch (Exception e) {
            throw new RuntimeException("Can't call method");
        }
    }
}
