package ru.mydesignstudio.container.utils;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public class ClassUtils {
    public static String normalizeClassName(Class<?> clazz) {
        return clazz.getSimpleName().substring(0, 1).toLowerCase() +
                clazz.getSimpleName().substring(1);
    }
}
