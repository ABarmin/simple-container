package ru.mydesignstudio.container.utils;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public class StringUtils {
    public static boolean isEmpty(String toCheck) {
        return toCheck == null || toCheck.isEmpty() || toCheck.trim().isEmpty();
    }

    public static boolean isNotEmpty(String toCheck) {
        return !isEmpty(toCheck);
    }
}
