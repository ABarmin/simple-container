package ru.mydesignstudio.container.post.processor;

import ru.mydesignstudio.container.context.ApplicationContext;
import ru.mydesignstudio.container.context.ApplicationContextAware;
import ru.mydesignstudio.container.context.Ordered;
import ru.mydesignstudio.container.context.Value;
import ru.mydesignstudio.container.definition.PropertyPlaceholderConfigurer;
import ru.mydesignstudio.container.utils.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.Arrays;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
@Ordered(Ordered.MIN_ORDER + 1)
public class ValueInjectionBeanPostProcessor implements BeanPostProcessor, ApplicationContextAware {
    private ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.context = applicationContext;
    }

    @Override
    public Object beforeInitialization(Object instance, String beanName) {
        Arrays.stream(instance.getClass().getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(Value.class))
                .forEach(field -> injectField(instance, field));

        return instance;
    }

    private void injectField(Object instance, Field field) {
        final String placeholderName = field.getAnnotation(Value.class).value()
                .replace("{", "")
                .replace("}", "");
        final PropertyPlaceholderConfigurer placeholderConfigurer = context.getBean(PropertyPlaceholderConfigurer.class);
        ReflectionUtils.setField(instance, field.getName(), placeholderConfigurer.getProperty(placeholderName));
    }
}
