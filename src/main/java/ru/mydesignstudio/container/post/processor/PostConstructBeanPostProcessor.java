package ru.mydesignstudio.container.post.processor;

import ru.mydesignstudio.container.context.ApplicationContext;
import ru.mydesignstudio.container.context.ApplicationContextAware;
import ru.mydesignstudio.container.context.Ordered;
import ru.mydesignstudio.container.definition.BeanDefinition;
import ru.mydesignstudio.container.utils.ReflectionUtils;
import ru.mydesignstudio.container.utils.StringUtils;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
@Ordered(Ordered.MIN_ORDER + 10)
public class PostConstructBeanPostProcessor implements BeanPostProcessor, ApplicationContextAware {
    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public Object beforeInitialization(Object instance, String beanName) {
        final BeanDefinition definition = applicationContext.getBeanDefinition(beanName);
        if (StringUtils.isNotEmpty(definition.getInitMethod())) {
            ReflectionUtils.invokeMethod(instance, definition.getInitMethod());
        }
        return instance;
    }
}
