package ru.mydesignstudio.container.post.processor;

import ru.mydesignstudio.container.context.Ordered;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public interface BeanPostProcessor {
    /**
     * Method is called before injection of all properties.
     *
     * @param instance bean instance
     * @param beanName bean name
     * @return configured instance
     */
    default Object beforeInitialization(Object instance, String beanName) {
        return instance;
    }

    /**
     * Method is called after injection of all properties.
     *
     * @param instance bean instance
     * @param beanName bean name
     * @return configured instance
     */
    default Object afterInitialization(Object instance, String beanName) {
        return instance;
    }
}
