package ru.mydesignstudio.container.post.processor;

import ru.mydesignstudio.container.context.ApplicationContext;
import ru.mydesignstudio.container.context.ApplicationContextAware;
import ru.mydesignstudio.container.context.Ordered;
import ru.mydesignstudio.container.definition.BeanDefinition;
import ru.mydesignstudio.container.definition.BeanPropertyDefinition;
import ru.mydesignstudio.container.utils.ReflectionUtils;
import ru.mydesignstudio.container.utils.StringUtils;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
@Ordered(Ordered.MIN_ORDER)
public class InjectionBeanPostProcessor implements BeanPostProcessor, ApplicationContextAware {
    private ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public Object beforeInitialization(Object instance, String beanName) {
        final BeanDefinition definition = applicationContext.getBeanDefinition(beanName);
        // go for all properties and try to inject dependencies
        definition.getProperties().stream()
                .forEach(property -> injectProperty(instance, property));
        return instance;
    }

    private void injectProperty(Object instance, BeanPropertyDefinition property) {
        if (StringUtils.isNotEmpty(property.getRef())) {
            final Object relatedBean = applicationContext.getBean(property.getRef());
            ReflectionUtils.setAccessible(instance, property.getName());
            ReflectionUtils.setField(instance, property.getName(), relatedBean);
        }
    }
}
