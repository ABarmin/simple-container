package ru.mydesignstudio.container.jdbc;

import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
@Named
public class JdbcTemplate {
    @Inject
    private DataSource dataSource;

    public void executeQuery(String query) {
        try (final Connection connection = dataSource.getConnection();) {
            final Statement statement = connection.createStatement();
            statement.execute(query);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public void executeUpdate(String query, Object... values) {
        try (final Connection connection = dataSource.getConnection()) {
            final PreparedStatement preparedStatement = connection.prepareStatement(query);
            prepareStatement(preparedStatement, values);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private void prepareStatement(PreparedStatement preparedStatement, Object[] values) throws SQLException {
        for (int i = 0; i < values.length; i++) {
            final Object value = values[i];
            if (value instanceof String) {
                preparedStatement.setString(i + 1, (String) value);
            } else if (value instanceof Integer) {
                preparedStatement.setInt(i + 1, (int) value);
            } else {
                throw new RuntimeException(String.format(
                        "Unsupported type %s",
                        value.getClass()
                ));
            }
        }
    }

    public <T> Collection<T> queryForList(String query, RowMapper<T> mapper, Object... values) {
        try (final Connection connection = dataSource.getConnection()) {
            final PreparedStatement preparedStatement = connection.prepareStatement(query);
            prepareStatement(preparedStatement, values);
            try (final ResultSet resultSet = preparedStatement.executeQuery()) {
                final Collection<T> result = new ArrayList<>();
                while (resultSet.next()) {
                    result.add(mapper.map(resultSet));
                }
                return result;
            }
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}
