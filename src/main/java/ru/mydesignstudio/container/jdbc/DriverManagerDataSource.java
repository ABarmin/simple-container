package ru.mydesignstudio.container.jdbc;

import ru.mydesignstudio.container.context.Value;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
@Named
public class DriverManagerDataSource implements DataSource {
    @Value("{jdbc.driver.class}")
    private String jdbcDriverClass;
    @Value("{jdbc.connection.string}")
    private String jdbcConnectionString;
    @Value("{jdbc.connection.login}")
    private String jdbcLogin;
    @Value("{jdbc.connection.password}")
    private String jdbcPassword;

    @PostConstruct
    public void init() {
        try {
            Class.forName(jdbcDriverClass);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        return getConnection(jdbcLogin, jdbcPassword);
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return DriverManager.getConnection(jdbcConnectionString, username, password);
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        throw new UnsupportedOperationException();
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        throw new UnsupportedOperationException();
    }
}
