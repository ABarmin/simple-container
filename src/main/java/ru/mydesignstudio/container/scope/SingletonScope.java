package ru.mydesignstudio.container.scope;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public class SingletonScope implements Scope {
    private final Map<String, Object> instances = new ConcurrentHashMap<>();

    @Override
    public String getScopeName() {
        return "singleton";
    }

    @Override
    public boolean hasInstance(String name) {
        return instances.containsKey(name);
    }

    @Override
    public void addInstance(String name, Object instance) {
        instances.put(name, instance);
    }

    @Override
    public void removeInstance(String name) {
        instances.remove(name);
    }

    @Override
    public Object getInstance(String name) {
        return instances.get(name);
    }
}
