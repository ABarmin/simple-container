package ru.mydesignstudio.container.scope;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public interface Scope {
    /**
     * Get scope name.
     *
     * @return scope name
     */
    String getScopeName();

    /**
     * Does scope contains instance with provided name.
     *
     * @param name instance name
     * @return contains or not
     */
    boolean hasInstance(String name);

    /**
     * Add instance to scope.
     *
     * @param name instance name
     * @param instance instance object
     */
    void addInstance(String name, Object instance);

    /**
     * Remove instance by name.
     *
     * @param name instance name
     */
    void removeInstance(String name);

    /**
     * Get instance from scope by name.
     *
     * @param name instance name
     * @return instance object
     */
    Object getInstance(String name);
}

