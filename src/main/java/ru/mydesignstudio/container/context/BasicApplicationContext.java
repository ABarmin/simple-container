package ru.mydesignstudio.container.context;

import ru.mydesignstudio.container.definition.BeanDefinition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public abstract class BasicApplicationContext implements ApplicationContext {
    private final Collection<BeanDefinition> definitions = new ArrayList<>();
    private final BeanFactory factoryDelegate;

    public BasicApplicationContext(final BeanDefinitionSource source) {
        this.read(source);

        factoryDelegate = new BeanFactoryDelegate(this, this);
    }

    @Override
    public void configure() {
        factoryDelegate.configure();
    }

    @Override
    public Collection<BeanDefinition> read(BeanDefinitionSource source) {
        definitions.clear();
        definitions.addAll(source.read());
        return definitions;
    }

    @Override
    public BeanDefinition getBeanDefinition(final String definitionName) {
        Objects.requireNonNull(definitionName, () -> "Definition wasn't provided");

        for (BeanDefinition definition : definitions) {
            if (isEquals(definition.getId(), definitionName)) {
                return definition;
            } else if (isContains(definition.getName(), definitionName)) {
                return definition;
            } else if (isEquals(definition.getBeanClass(), definitionName)) {
                return definition;
            }
        }

        throw new IllegalArgumentException("There is no definition with name " + definitionName);
    }

    @Override
    public <T> Collection<BeanDefinition> getBeanDefinition(Class<T> definitionClass) {
        return definitions.stream()
                .filter(definition -> definition.getInterfaces().contains(definitionClass.getCanonicalName()))
                .sorted(new OrderedComparator())
                .collect(Collectors.toList());
    }

    @Override
    public Object getBean(String beanName) {
        return factoryDelegate.getBean(beanName);
    }

    @Override
    public <T> T getBean(Class<T> beanClass) {
        return factoryDelegate.getBean(beanClass);
    }

    @Override
    public <T> T getBean(String beanName, Class<T> beanClass) {
        return factoryDelegate.getBean(beanName, beanClass);
    }

    private boolean isContains(Collection<String> collection, String toFind) {
        for (String item : collection) {
            if (isEquals(item, toFind)) {
                return true;
            }
        }
        return false;
    }

    private boolean isEquals(String string1, String string2) {
        return string1 != null && string2 != null &&
                string1.equalsIgnoreCase(string2);
    }
}
