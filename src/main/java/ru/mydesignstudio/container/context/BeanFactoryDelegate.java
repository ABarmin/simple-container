package ru.mydesignstudio.container.context;

import ru.mydesignstudio.container.definition.BeanDefinition;
import ru.mydesignstudio.container.post.processor.BeanPostProcessor;
import ru.mydesignstudio.container.scope.Scope;
import ru.mydesignstudio.container.scope.SingletonScope;
import ru.mydesignstudio.container.utils.ClassUtils;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
@SuppressWarnings("unchecked")
public class BeanFactoryDelegate implements BeanFactory, Configurable {
    private final BeanDefinitionRegistry registry;
    private final ApplicationContext applicationContext;
    private final Set<BeanPostProcessor> postProcessors = new LinkedHashSet<>();
    private final Map<String, Scope> scopes = new HashMap<>();

    public BeanFactoryDelegate(final BeanDefinitionRegistry registry, final ApplicationContext applicationContext) {
        this.registry = registry;
        this.applicationContext = applicationContext;
    }

    @Override
    public void configure() {
        // configure all scopes
        scopes.put("singleton", new SingletonScope());
        registry.getBeanDefinition(Scope.class)
                .stream()
                .map(this::getBean)
                .map(s -> (Scope) s)
                .forEach(scope -> scopes.put(scope.getScopeName(), scope));

        // configure all bean post processors
        registry.getBeanDefinition(BeanPostProcessor.class)
                .stream()
                .map(this::getBean)
                .map(bpp -> (BeanPostProcessor) bpp)
                .forEach(bpp -> postProcessors.add(bpp));
    }

    private <T> T instantiate(BeanDefinition definition) {
        try {
            // todo, don't instantiate twice
            final Class<?> aClass = Class.forName(definition.getBeanClass());
            return (T) aClass.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Object getBean(final BeanDefinition definition) {
        // try to get bean from scope's cache
        Object instance = getBeanFromScope(definition);
        if (instance != null) {
            return instance;
        }
        instance = instantiate(definition);
        // add bean to scope's cache
        addBeanToScope(definition, instance);
        // run built-in post processors
        if (instance instanceof ApplicationContextAware) {
            ((ApplicationContextAware) instance).setApplicationContext(applicationContext);
        }
        // run bean post processors
        for (BeanPostProcessor postProcessor : postProcessors) {
            postProcessor.beforeInitialization(instance, definition.getId());
        }
        for (BeanPostProcessor postProcessor : postProcessors) {
            postProcessor.afterInitialization(instance, definition.getId());
        }
        return instance;
    }

    private void addBeanToScope(BeanDefinition definition, Object instance) {
        final Scope scope = scopes.get(definition.getScope());
        if (scope == null) {
            throw new RuntimeException("There is no scope with name " + definition.getScope());
        }
        scope.addInstance(definition.getId(), instance);
    }

    private Object getBeanFromScope(final BeanDefinition definition) {
        final Scope scope = scopes.get(definition.getScope());
        if (scope == null) {
            throw new RuntimeException("There is no scope with name " + definition.getScope());
        }
        if (scope.hasInstance(definition.getId())) {
            return scope.getInstance(definition.getId());
        }
        return null;
    }

    @Override
    public Object getBean(String beanName) {
        final BeanDefinition definition = registry.getBeanDefinition(beanName);
        return getBean(definition);
    }

    @Override
    public <T> T getBean(Class<T> beanClass) {
        final BeanDefinition definition = registry.getBeanDefinition(ClassUtils.normalizeClassName(beanClass));
        return (T) getBean(definition);
    }

    @Override
    public <T> T getBean(String beanName, Class<T> beanClass) {
        final BeanDefinition definition = registry.getBeanDefinition(beanName);
        return (T) getBean(definition);
    }
}
