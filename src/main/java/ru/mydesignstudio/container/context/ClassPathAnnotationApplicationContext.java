package ru.mydesignstudio.container.context;

import ru.mydesignstudio.container.definition.BeanDefinition;

import java.util.Collection;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public class ClassPathAnnotationApplicationContext extends BasicApplicationContext {
    public ClassPathAnnotationApplicationContext() {
        super(new ClassPathAnnotationBeadDefinitionSource());
        configure();
    }
}
