package ru.mydesignstudio.container.context;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Value {
    String value() default "";
}
