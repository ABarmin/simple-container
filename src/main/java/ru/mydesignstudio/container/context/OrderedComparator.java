package ru.mydesignstudio.container.context;

import ru.mydesignstudio.container.definition.BeanDefinition;

import java.util.Comparator;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public class OrderedComparator implements Comparator<BeanDefinition> {
    @Override
    public int compare(BeanDefinition o1, BeanDefinition o2) {
        return Integer.compare(o1.getOrder(), o2.getOrder());
    }
}
