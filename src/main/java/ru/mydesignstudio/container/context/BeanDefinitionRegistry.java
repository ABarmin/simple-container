package ru.mydesignstudio.container.context;

import ru.mydesignstudio.container.definition.BeanDefinition;

import java.util.Collection;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public interface BeanDefinitionRegistry {
    /**
     * Reads definitions from some source.
     *
     * @param source source to read from
     * @return collection of bean definitions
     */
    Collection<BeanDefinition> read(BeanDefinitionSource source);

    /**
     * Get bean definition by id or name.
     *
     * @param definitionName definition name
     * @return bean definition
     */
    BeanDefinition getBeanDefinition(String definitionName);

    /**
     * Get bean definitions by provided interface.
     * @param definitionClass definition interface
     * @param <T> type of interface
     * @return collection of definitions
     */
    <T> Collection<BeanDefinition> getBeanDefinition(Class<T> definitionClass);
}
