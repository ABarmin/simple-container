package ru.mydesignstudio.container.context;

import ru.mydesignstudio.container.definition.BeanDefinition;
import ru.mydesignstudio.container.definition.BeanPropertyDefinition;
import ru.mydesignstudio.container.utils.ClassUtils;
import ru.mydesignstudio.container.utils.StringUtils;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.stream.Collectors;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public class ClassPathAnnotationBeadDefinitionSource implements BeanDefinitionSource {
    @Override
    public Collection<BeanDefinition> read() {
        final Collection<BeanDefinition> definitions = new ArrayList<>();

        try {
            final Enumeration<URL> roots = getClass().getClassLoader().getResources("");
            while (roots.hasMoreElements()) {
                definitions.addAll(read(roots.nextElement()));
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return definitions;
    }

    private Collection<BeanDefinition> read(URL url) {
        final File root = new File(url.getPath());
        return read(root, root.getPath());
    }

    private Collection<BeanDefinition> read(File file, String basePath) {
        final Collection<BeanDefinition> definitions = new ArrayList<>();
        if (file.isDirectory()) {
            for (File internalFile : file.listFiles()) {
                definitions.addAll(read(internalFile, basePath));
            }
        } else if (file.isFile() && isClassFile(file)) {
            definitions.add(readDefinition(file, basePath));
        }
        return definitions;
    }

    private boolean isClassFile(File file) {
        return file.getName().endsWith(".class") && !file.getName().contains("$");
    }

    private BeanDefinition readDefinition(File file, String basePath) {
        // get full class name by class file
        final String className = file.getPath().replace(basePath, "")
                .replace(".class", "")
                .replace("/", ".")
                .substring(1);
        // read class
        try {
            final Class<?> forName = Class.forName(className);
            return readDefinition(forName);
        } catch (Exception e) {
            throw new RuntimeException("Failed on class " + className, e);
        }
    }

    private BeanDefinition readDefinition(Class<?> clazz) {
        final BeanDefinition definition = new BeanDefinition();
        definition.setId(getBeanId(clazz));
        definition.setName(getBeanNames(clazz));
        definition.setInitMethod(getBeanInitMethod(clazz));
        definition.setDestroyMethod(getBeanDestroyMethod(clazz));
        definition.setProperties(getBeanProperties(clazz));
        definition.setBeanClass(clazz.getCanonicalName());
        definition.setInterfaces(getBeanInterfaces(clazz));
        definition.setScope(getBeanScope(clazz));
        definition.setOrder(getBeanOrder(clazz));
        return definition;
    }

    private int getBeanOrder(Class<?> clazz) {
        if (clazz.isAnnotationPresent(Ordered.class)) {
            return clazz.getAnnotation(Ordered.class).value();
        }
        return Ordered.DEFAULT_ORDER;
    }

    private String getBeanScope(Class<?> clazz) {
        return "singleton"; // todo, let it be for the moment
    }

    private Collection<String> getBeanInterfaces(Class<?> clazz) {
        return Arrays.stream(clazz.getInterfaces())
                .map(Class::getCanonicalName)
                .collect(Collectors.toList());
    }

    private Collection<BeanPropertyDefinition> getBeanProperties(Class<?> clazz) {
        return Arrays.stream(clazz.getDeclaredFields())
                .filter(field -> field.isAnnotationPresent(Inject.class))
                .map(this::toPropertyDefinition)
                .collect(Collectors.toList());
    }

    private BeanPropertyDefinition toPropertyDefinition(Field field) {
        final BeanPropertyDefinition definition = new BeanPropertyDefinition();
        definition.setName(field.getName());
        definition.setRef(getPropertyValue(field));
        return definition;
    }

    private String getPropertyValue(Field field) {
        // todo, I think, there should be some value resolver
        return ClassUtils.normalizeClassName(field.getType());
    }

    private String getBeanDestroyMethod(Class<?> clazz) {
        return Arrays.stream(clazz.getDeclaredMethods())
                .filter(method -> method.isAnnotationPresent(PreDestroy.class))
                .map(Method::getName)
                .findFirst()
                .orElseGet(() -> null);
    }

    private String getBeanInitMethod(Class<?> clazz) {
        return Arrays.stream(clazz.getDeclaredMethods())
                .filter(method -> method.isAnnotationPresent(PostConstruct.class))
                .map(Method::getName)
                .findFirst()
                .orElseGet(() -> null);
    }

    private Collection<String> getBeanNames(Class<?> clazz) {
        return Arrays.stream(clazz.getInterfaces())
                .map(ClassUtils::normalizeClassName)
                .collect(Collectors.toList());
    }

    private String getBeanId(Class<?> clazz) {
        // try to use @Named annotation to get bean's id
        if (clazz.isAnnotationPresent(Named.class)
                && StringUtils.isNotEmpty(clazz.getAnnotation(Named.class).value())) {
            return clazz.getAnnotation(Named.class).value();
        } else {
            return ClassUtils.normalizeClassName(clazz);
        }
    }


}
