package ru.mydesignstudio.container.context;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public interface ApplicationContextAware {
    /**
     * Set applicationContext to something.
     *
     * @param applicationContext applicationContext instance
     */
    void setApplicationContext(ApplicationContext applicationContext);
}
