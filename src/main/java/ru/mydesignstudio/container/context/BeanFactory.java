package ru.mydesignstudio.container.context;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
public interface BeanFactory extends Configurable {
    /**
     * Get bean by bean's name.
     *
     * @param beanName bean's name
     * @return prepared bean instance
     */
    Object getBean(String beanName);

    /**
     * Get bean by name and class.
     *
     * @param beanName bean's name
     * @param beanClass bean's class
     * @param <T> bean's type
     * @return prepared bean instance
     */
    <T> T getBean(String beanName, Class<T> beanClass);

    /**
     * Get bean by bean's class. Throws {@link IllegalArgumentException} if there are more
     * than one bean implements provided interface.
     *
     * @param beanClass bean's class
     * @param <T> type of bean
     * @return prepared bean instance
     */
    <T> T getBean(Class<T> beanClass);
}
