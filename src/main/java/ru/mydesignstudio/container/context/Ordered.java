package ru.mydesignstudio.container.context;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This code was written by Aleksandr Barmin.
 * If you have any questions, feel free to write me a letter at
 * <a href="mailto:abarmin@outlook.com">abarmin@outlook.com</a>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Ordered {
    int DEFAULT_ORDER = 0;
    int MIN_ORDER = Integer.MIN_VALUE;
    int MAX_ORDER = Integer.MAX_VALUE;

    /**
     * Get order of something.
     *
     * @return
     */
    int value() default DEFAULT_ORDER;
}
